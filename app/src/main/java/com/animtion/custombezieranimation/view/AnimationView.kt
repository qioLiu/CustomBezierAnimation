package com.animtion.custombezieranimation.view

import android.animation.*
import android.content.Context
import android.util.AttributeSet
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import com.animtion.custombezieranimation.R

/**
 * create for
 * Created by six.sev on 2017/8/1.
 */
class AnimationView : RelativeLayout {
    val dp80 = UIUtils.dp2px(context, 80f)
    var mWidth = 0
    var mHeight = 0

    var redImage: ImageView? = null
    var purpleImage: ImageView? = null
    var yellowImage: ImageView? = null
    var blueImage: ImageView? = null

    var redPath: ViewPath? = null
    var purplePath: ViewPath? = null
    var yellowPath: ViewPath? = null
    var bluePath: ViewPath? = null

    var redAnimationSet: AnimatorSet? = null
    var purpleAnimationSet: AnimatorSet? = null
    var yellowAnimationSet: AnimatorSet? = null
    var blueAnimationSet: AnimatorSet? = null

    constructor(context: Context) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        mWidth = measuredWidth
        mHeight = measuredHeight
        initPath()
    }

    private fun initPath() {
        redPath = ViewPath()
        redPath!!.moveTo(0f, 0f)
        redPath!!.lineTo((mWidth / 5 - mWidth / 2).toFloat(), 0f)
        redPath!!.cubicTo(-700f, (-mHeight / 2).toFloat(), (mWidth / 3 * 2).toFloat(), (-mHeight / 3 * 2).toFloat(), 0f, -dp80.toFloat())

        purplePath = ViewPath()
        purplePath!!.moveTo(0f, 0f)
        purplePath!!.lineTo((mWidth / 5 * 2 - mWidth / 2).toFloat(), 0f)
        purplePath!!.cubicTo(-300f, (-mHeight / 2).toFloat(), mWidth.toFloat(), (-mHeight / 9 * 5).toFloat(), 0f, -dp80.toFloat())

        yellowPath = ViewPath()
        yellowPath!!.moveTo(0f, 0f)
        yellowPath!!.lineTo((mWidth / 5 * 3 - mWidth / 2).toFloat(), 0f)
        yellowPath!!.cubicTo(300f, mHeight.toFloat(), -mWidth.toFloat(), (-mHeight / 9 * 5).toFloat(), 0f, -dp80.toFloat())

        bluePath = ViewPath()
        bluePath!!.moveTo(0f, 0f)
        bluePath!!.lineTo((mWidth / 5 * 4 - mWidth / 2).toFloat(), 0f)
        bluePath!!.cubicTo(700f, (mHeight / 3 * 2).toFloat(), (-mWidth / 2).toFloat(), (mHeight / 2).toFloat(), 0f, -dp80.toFloat())
    }

    fun initView(){
        var params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        params.addRule(CENTER_IN_PARENT)

        redImage = ImageView(context)
        redImage!!.setImageResource(R.drawable.red)
        redImage!!.layoutParams = params
        addView(redImage)

        purpleImage = ImageView(context)
        purpleImage!!.setImageResource(R.drawable.purple)
        purpleImage!!.layoutParams = params
        addView(purpleImage)

        yellowImage = ImageView(context)
        yellowImage!!.setImageResource(R.drawable.yellow)
        yellowImage!!.layoutParams = params
        addView(yellowImage)

        blueImage = ImageView(context)
        blueImage!!.setImageResource(R.drawable.blue)
        blueImage!!.layoutParams = params
        addView(blueImage)

        setAnimation(redImage, redPath)
        setAnimation(purpleImage, purplePath)
        setAnimation(yellowImage, yellowPath)
        setAnimation(blueImage, bluePath)
    }

    private fun setAnimation(image: ImageView?, viewPath: ViewPath?) {
        /**
         * 这里注意ofObject最后一个参数类型是vararg， 代表多个参数， 在kotlin中如果要用Array数组来表示的话要在前面加上*，不然无法使用。
         */
        var objAni: ObjectAnimator = ObjectAnimator.ofObject(ViewObj(image!!), "fabLoc", CustomEvaluator(), *(viewPath!!.getPointArray()))
        objAni.interpolator = AccelerateDecelerateInterpolator()
        objAni.duration = 2600
        addScaleAnimtion(objAni, image)
    }

    private fun addScaleAnimtion(objAni: ObjectAnimator, image: ImageView) {
        var scaleAni: ValueAnimator = ValueAnimator.ofFloat(1f, 1000f)
        scaleAni.duration = 1800
        scaleAni.startDelay = 1000
        scaleAni.addUpdateListener { animation ->
            var scale = getScale(image) - 1
            var value = animation!!.animatedValue as Float
            var alpha = 1 - value / 2000
            if(value <= 500){
                scale = 1 + value / 500 * scale
            }else{
                scale = 1 + (1000 - value) / 500 * scale
            }
            image.scaleX = scale
            image.scaleY = scale
            image.alpha = alpha
        }
        scaleAni.addListener(AnimationEndLisener(image!!))
        when(image){
            redImage -> {redAnimationSet = AnimatorSet()
                redAnimationSet!!.playTogether(objAni, scaleAni)}
            purpleImage -> {purpleAnimationSet = AnimatorSet()
                purpleAnimationSet!!.playTogether(objAni, scaleAni)}
            yellowImage -> {yellowAnimationSet = AnimatorSet()
                yellowAnimationSet!!.playTogether(objAni, scaleAni)}
            blueImage -> {blueAnimationSet = AnimatorSet()
                blueAnimationSet!!.playTogether(objAni, scaleAni)}
            else -> null
        }
    }

    fun start(){
        removeAllViews()
        initView()
        redAnimationSet!!.start()
        purpleAnimationSet!!.start()
        yellowAnimationSet!!.start()
        blueAnimationSet!!.start()
    }

    fun getScale(image: ImageView): Float{
        when(image){
            redImage -> 3f
            purpleImage -> 2f
            yellowImage -> 3.5f
            blueImage -> 4.5f
            else -> 2.0f
        }
        return 2.0f
    }

    inner class ViewObj(var image: ImageView){
        fun setFabLoc(point: ViewPoint){
            image.translationX = point.x
            image.translationY = point.y
        }
    }

    inner class AnimationEndLisener(var image: ImageView) : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator?) {
            super.onAnimationEnd(animation)
            removeView(image)
        }
    }

}
package com.animtion.custombezieranimation.view

import android.animation.TypeEvaluator

/**
 * create for
 * Created by six.sev on 2017/8/1.
 */
class CustomEvaluator : TypeEvaluator<ViewPoint>{
    var startX = 0f
    var startY = 0f
    var x = 0f
    var y = 0f
    override fun evaluate(fraction: Float, startValue: ViewPoint?, endValue: ViewPoint?): ViewPoint {
        when(endValue!!.operation){
            ViewPath.MOVE ->{
                x = endValue.x
                y = endValue.y
            }
            ViewPath.LINE -> {
                startX = if(startValue!!.operation == ViewPath.QUAD) startValue.x1 else startValue.x
                startX = if(startValue!!.operation == ViewPath.CUBIC) startValue.x2 else startX
                startY = if(startValue!!.operation == ViewPath.QUAD) startValue.y1 else startValue.y
                startY = if(startValue!!.operation == ViewPath.CUBIC) startValue.y2 else startY

                x = startX + fraction * (endValue.x - startX)
                y = startY+ fraction * (endValue.y - startY)
            }
            ViewPath.QUAD -> {
                startX = if(startValue!!.operation == ViewPath.CUBIC) startValue.x2 else startValue.x
                startY = if(startValue!!.operation == ViewPath.CUBIC) startValue.y2 else startValue.y

                var oneMinusT = 1 - fraction
                x = oneMinusT * oneMinusT *  startX +
                        2 * oneMinusT *  fraction * endValue.x +
                        fraction * fraction * endValue.x1

                y = oneMinusT * oneMinusT * startY +
                        2  * oneMinusT * fraction * endValue.y +
                        fraction * fraction * endValue.y1
            }
            ViewPath.CUBIC -> {
                startX = if(startValue!!.operation == ViewPath.QUAD) startValue.x1 else startValue.x
                startY = if(startValue!!.operation == ViewPath.QUAD) startValue.y1 else startValue.y

                var oneMinusT = 1 - fraction
                x = oneMinusT * oneMinusT * oneMinusT * startX +
                        3 * oneMinusT * oneMinusT * fraction * endValue.x +
                        3 * oneMinusT * fraction * fraction * endValue.x1+
                        fraction * fraction * fraction * endValue.x2

                y = oneMinusT * oneMinusT * oneMinusT * startY +
                        3 * oneMinusT * oneMinusT * fraction * endValue.y +
                        3 * oneMinusT * fraction * fraction * endValue.y1+
                        fraction * fraction * fraction * endValue.y2
            }
            else -> {
                x = endValue.x
                y = endValue.y
            }
        }
        return ViewPoint(x, y)
    }
}
package com.animtion.custombezieranimation.view

/**
 * create for point collection
 * Created by six.sev on 2017/8/1.
 */
class ViewPath {
    companion object{
        val MOVE = 0
        val LINE = 1
        val QUAD = 2
        val CUBIC = 3
    }

    var points: MutableList<ViewPoint?>? = null
    init {
        points = mutableListOf<ViewPoint?>()
    }

    fun getPointArray(): Array<ViewPoint?>{
        var size = points!!.size
        var arrays = arrayOfNulls<ViewPoint>(size)
        for (index in 0..size - 1){
            arrays.set(index, points!!.get(index))
        }
        return arrays
    }

    fun moveTo(x: Float, y: Float){
        points!!.add(ViewPoint.movePoint(x, y, MOVE))
    }

    fun lineTo(x: Float, y: Float){
        points!!.add(ViewPoint.linePoint(x, y, LINE))
    }

    fun quadTo(x: Float, y: Float, x1: Float, y1: Float){
        points!!.add(ViewPoint.quadPoint(x, y, x1, y1, QUAD))
    }

    fun cubicTo(x: Float, y: Float, x1: Float, y1: Float, x2: Float, y2: Float){
        points!!.add(ViewPoint.cubicPoint(x, y, x1, y1, x2, y2, CUBIC))
    }
}